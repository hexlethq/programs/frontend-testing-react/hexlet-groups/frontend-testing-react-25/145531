const fs = require('fs');
const path = require('path');
const os = require('os');
const { upVersion } = require('../src/index');

// BEGIN

const getFixturePath = (fixtureName) => path.join('__fixtures__', fixtureName);
const readJson = (jsonPath) => JSON.parse(fs.readFileSync(jsonPath, 'utf8'));

let targetPath;

beforeEach(() => {
  const tempPath = fs.mkdtempSync(path.join(os.tmpdir(), 'tests-'));

  const fixturePath = getFixturePath('package.json');
  const fixture = fs.readFileSync(fixturePath, 'utf8');

  targetPath = path.join(tempPath, 'package.json');

  fs.writeFileSync(targetPath, fixture);
});

const table = [
  {
    inc: undefined,
    expected: '1.3.3',
  },
  {
    inc: 'patch',
    expected: '1.3.3',
  },
  {
    inc: 'minor',
    expected: '1.4.2',
  },
  {
    inc: 'major',
    expected: '2.3.2',
  },
];

test.each(table)('start version: 1.3.2, increment: $inc, expected: $expected', ({ inc, expected }) => {
  upVersion(targetPath, inc);
  expect(readJson(targetPath)).toMatchObject({ version: expected });
});

test('throws error if path to package.json is wrong', () => {
  expect(() => upVersion('some-wrong-path')).toThrow();
});

test('affects only version property', () => {
  const { version: initialVersion, ...initialJSON } = readJson(targetPath);

  upVersion(targetPath);
  upVersion(targetPath, 'major');
  upVersion(targetPath, 'minor');
  upVersion(targetPath, 'patch');

  const { version: processedVersion, ...processedJSON } = readJson(targetPath);

  expect(initialJSON).toStrictEqual(processedJSON);
});

// END
