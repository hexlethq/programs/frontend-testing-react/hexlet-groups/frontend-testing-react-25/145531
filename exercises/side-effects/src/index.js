const fs = require('fs');

// BEGIN
const DEFAULT_INC = 'patch';

const upVersionStr = (version, inc = DEFAULT_INC) => {
  const [major, minor, patch] = version.split('.');

  const map = new Map([
    ['major', Number(major)],
    ['minor', Number(minor)],
    ['patch', Number(patch)],
  ]);

  map.set(inc, map.get(inc) + 1);

  return [...map.values()].join('.');
};

const upVersion = (filePath, inc = DEFAULT_INC) => {
  const json = JSON.parse(fs.readFileSync(filePath, 'utf8'));
  const { version } = json;

  json.version = upVersionStr(version, inc);

  fs.writeFileSync(filePath, JSON.stringify(json));
};
// END

module.exports = { upVersion };
