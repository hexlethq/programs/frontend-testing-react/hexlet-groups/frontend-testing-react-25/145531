const faker = require('faker');

// BEGIN
describe('createTransaction', () => {
  test('creates transaction object with specific properties', () => {
    const transaction = faker.helpers.createTransaction();

    expect(transaction).toStrictEqual({
      amount: expect.any(String),
      date: expect.any(Date),
      business: expect.any(String),
      name: expect.any(String),
      type: expect.any(String),
      account: expect.any(String),
    });
  });

  test('creates random data', () => {
    const transaction1 = faker.helpers.createTransaction();
    const transaction2 = faker.helpers.createTransaction();

    expect(transaction1).not.toEqual(transaction2);
  });

  test('amount and account properties can be considered as numbers', () => {
    const transaction = faker.helpers.createTransaction();
    const { amount, account } = transaction;

    expect(Number(amount)).not.toBeNaN();
    expect(Number(account)).not.toBeNaN();
  });
});
// END
