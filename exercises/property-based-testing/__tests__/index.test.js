const fc = require('fast-check');

const sort = (data) => data.slice().sort((a, b) => a - b);

// BEGIN
describe('sort', () => {
  test('sort arr with numbers in ascending order', () => {
    fc.assert(
      fc.property(
        fc.int8Array(),
        (arr) => expect(sort(arr)).toBeSorted({ descending: false }),
      ),
    );
  });

  test('sort arr with numbers as string type in ascending lexicographic order', () => {
    fc.assert(
      fc.property(
        fc.array(fc.integer().map(String)),
        (arr) => expect(sort(arr)).toBeSorted({ coerce: true }),
      ),
    );
  });

  test('sort is idempotent', () => {
    fc.assert(
      fc.property(
        fc.int8Array(),
        (arr) => expect(arr.sort().sort()).toEqual(arr.sort()),
      ),
    );
  });

  test('sort returns new pointer', () => {
    const src = [3, 5, 4];
    const sorted = sort(src);

    expect(src).not.toBe(sorted);
  });
});
// END
