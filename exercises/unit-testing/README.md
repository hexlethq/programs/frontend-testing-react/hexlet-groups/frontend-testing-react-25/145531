# Unit-тестирование

Протестируйте функцию `Object.assign()`. 

У данной функции есть особенность, он изменяет только целевой объект. Не забудьте отразить этот момент в тестах.

## Ссылки

* [Object.assign()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)
