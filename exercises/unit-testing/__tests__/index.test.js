test('main', () => {
  const src = { k: 'v', b: 'b' };
  const target = { k: 'v2', a: 'a' };
  const result = Object.assign(target, src);

  expect.hasAssertions();

  // BEGIN
  expect(result).toEqual({ k: 'v', b: 'b', a: 'a' });
});

test('target object is the same value as returned result', () => {
  const src = {};
  const target = { a: 'a', b: 'b' };
  const result = Object.assign(target, src);

  expect(result).toBe(target);
});

test('inherited properties are not copied', () => {
  const src = Object.create({ b: 'b' }, {
    a: {
      value: 'a',
      enumerable: true,
    },
  });
  const target = { c: 'c' };
  const result = Object.assign(target, src);

  expect(result).toEqual({ a: 'a', c: 'c' });
});

test('only enumerable properties are copied', () => {
  const src = Object.create({}, {
    bar: {
      value: 1,
    },
    baz: {
      value: 2,
      enumerable: true,
    },
    qux: {
      value: 3,
      enumerable: true,
    },
  });

  const target = {};
  const result = Object.assign(target, src);

  expect(result).toEqual({ baz: 2, qux: 3 });
});

test('error throwing on a copy attempt to non writable property', () => {
  const target = Object.defineProperty({}, 'notWritable', {
    value: 1,
    writable: false,
  });

  const src = { notWritable: 2 };
  expect(() => Object.assign(target, src)).toThrow();
});
// END
